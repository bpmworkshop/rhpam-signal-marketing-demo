Red Hat Process Automation Manager Signal Marketing Demo
========================================================
This demo is a financial process with human task integration, rule integration, task forms, and features
the use of a signal to add a customer contact moment for the marketing department.


Install on your machine
-----------------------
1. [Download and unzip.](https://gitlab.com/bpmworkshop/rhpam-signal-marketing-demo/-/archive/main/rhpam-signal-marketing-demo-main.zip)

2. Add products to installs directory, see installs/README for details and links.

3. Run 'init.sh' or 'init.bat' file. 'init.bat' must be run with Administrative privileges, follow displayed instructions to start demo.

Login to http://localhost:8080/business-central (for admin and Loan Officer roles use u:erics / p:redhatpam1!)


Install in a local container (Podman)
-------------------------------------
1. [Download and unzip.](https://gitlab.com/bpmworkshop/rhpam-signal-marketing-demo/-/archive/main/rhpam-signal-marketing-demo-main.zip)

2. Add products to installs directory, see installs/README for details and links.

3. [Install Podman on your machine](https://podman.io/getting-started/installation), the following instructions are OSX as an example.

4. Set up the virtual machine with 6GB memory:

  ```
  $ podman machine init --memory 6144 --disk-size 20
  ```

5. Ensure you have the proper networking setup for rootless containers:

  ```
   # In file ~/.config/containers/containers.conf make sure the CONTAINERS section 
   # has the line shown here:

   [containers]
   rootless_networking = "cni"
  ```

6. Start the virtual machine:

  ```
  $ podman machine start
  ```

7. Build the Red Hat Process Automation Manager container image from the root directory of the project:

  ```
  $ podman build -t rhpam-signal-marketing:7.11 .
  ```

8. Verify image has been added to the local container regsitry:

  ```
  $ podman image list

   REPOSITORY                            TAG         IMAGE ID      CREATED         SIZE
   localhost/rhpam-signal-marketing      7.11        5af089224c65  26 minutes ago  4.05 GB
   docker.io/jbossdemocentral/developer  latest      b73501ac39b1  5 years ago     514 MB
  ```

9. Run the rhpam-install image with ports mapped for access:

  ```
  $ podman run -dt -p 8080:8080 -p 9990:9990 rhpam-signal-marketing
  ```

10. Verify logs of container that it's started before logging in (find CONTAINER_ID with 'podman container list':

  ```
  $ podman logs -f [CONTAINER_ID]
  ```

Login to http://localhost:8080/business-central (for admin and Loan Officer roles use u:erics / p:redhatpam1!)

Enjoy installed and configured Red Hat Process Automation Manager.


Demo run
--------
1. Open project "Generic Loan Demo" in projects. 


2. The project has a data model (Eligible and LoanApplication), task forms, several rules, and a generic loan application process.


3. Build and deploy version 1.0 of the project by clicking on the "Deploy" button in the upper right corner.


4. Go to "Menu -> Deploy -> Execution Servers" repository to see the loan_1.0 Kie Container deployed on the Process Automation Server.


5. Go to "Menu -> Manage -> Process Definitions" to see the GenericLoanProcess version 1.1 has been deployed.


6. Click on the process definition line, then start a New Process Instance with button in the top right corner.


7. Fill in the start process form as follows and click on Submit button:

  ```
  Name: Eric D Schabell
  Age: 25
  Annual income: 90000
  Loan Amount: 10010    (auto rejected if value under 10k)
  Loan Period (months): 10
  ```

8. Go to "Menu -> Manage -> Process Instances" and select the process instance listed as running that you just started.
This give the options to look at Instance Details, Process Variables, Documents, Logs, and Diagrams. Let's look at where 
this process is right now by selecting the Diagrams tab.

![Approve Task](docs/demo-images/approve-task.png)

9. The red box around the "Approve" task means we need to decide if the loan requested is to be approved or not, so go to 
the "Menu -> Track -> Task inbox" and selecting the waiting task. Note you can see this taks if you are logged in as a user
in the "LoanOfficer" group, which user "erics" is part of (see intallation script for add user details).


10 The task has been assigned to your the 'loanOfficer' group, so start by clicking on the "Claim" task button, then "Start" 
task button. These are standards-based user task completion steps using these phase labels. Now you can edit the only available 
task form field, the Approve box (or leave blank to reject the request). 


11. After submitting the form by clicking on the "Complete" button go to "Menu -> Manage ->  Process Instances" and in the filters 
panel on the left put a check-box in the "Completed" field to view completed process instances. Find your just completed process 
instance, select it from the list, and open the "Diagram" tab to view the greyed out boxes that depict the path taken through this 
process. Below you see an example where the application has completed in the "End Denied" final end node of the process and that a
signal was sent to complete the marketing customer contact (CC) activity before ending in "End CC" end node.

![End Process](docs/demo-images/end-process-denied.png)

12. You can explore running the process with a loan request below 10k and possibly changing the input variables for other outcomes.


Note
----
Kie Servier access configured (u: kieserver / p:kieserver1!). Enjoy installed and configured Red Hat Process Automation  Manager.

View the API docs at: http://localhost:8080/kie-server/docs/

Source for demo client available in projects directory.


Optional - Install on OpenShift Container Platform
--------------------------------------------------
See the following project to install in a container on OpenShift Container Platform:

  - [CodeReady Containers Signal Marketing Process](https://gitlab.com/redhatdemocentral/crc-signal-marketing-demo) 


Supporting Articles
-------------------
- [Codeanywhere adventures - Creating your first container project (part 2)](https://www.schabell.org/2021/09/codeanywhere-adventures-creating-your-first-container-project-part2.html)

- [Beginners Guide to Installing Process Automation Tooling in a Local Container using Podman](https://www.schabell.org/2021/09/beginners-guide-to-rhpam-local-conainter-podman.html)

- [4 Easy Steps for Migrating Projects to OpenShift Container Platform](https://www.schabell.org/2021/03/4-easy-steps-for-migrating-projects-to-openshift-container-platform.html)


Released versions
-----------------
See the tagged releases for the following versions of the product:

- v1.4 - Red Hat Process Automation Manager 7.11.0 and pre-installed signal marketing demo with updated signal process installed locally with podman container support.

- v1.3 - Red Hat Process Automation Manager 7.11.0 and pre-installed signal marketing demo with updated signal process.

- v1.2 - Red Hat Process Automation Manager 7.10.0 and pre-installed signal marketing demo with updated signal process.

- v1.1 - Red Hat Process Automation Manager 7.9.0 and pre-installed signal marketing demo with updated signal process.

- v1.0 - Red Hat Process Automation Manager 7.9.0 and pre-installed signal marketing demo.


![Start Form](docs/demo-images/loan-application-startform.png)

![Approve Task](docs/demo-images/approve-task.png)

![Approve Task Form](docs/demo-images/approve-loan-taskform.png)

![Start Process](docs/demo-images/end-process-approved.png)

