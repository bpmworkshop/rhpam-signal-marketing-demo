# Use jbossdemocentral/developer as the base
FROM docker.io/jbossdemocentral/developer

# Maintainer details
MAINTAINER Andrew Block, Eric D. Schabell

# Environment Variables 
ENV RHPAM_HOME /opt/jboss/rhpam/jboss-eap-7.3
ENV RHPAM_VERSION_MAJOR 7
ENV RHPAM_VERSION_MINOR 11
ENV RHPAM_VERSION_MICRO 0

ENV EAP_VERSION_MAJOR 7
ENV EAP_VERSION_MINOR 3
ENV EAP_VERSION_MICRO 0

ENV EAP_ZIP=jboss-eap-$EAP_VERSION_MAJOR.$EAP_VERSION_MINOR.$EAP_VERSION_MICRO.zip
ENV RHPAM_DEPLOYABLE=rhpam-$RHPAM_VERSION_MAJOR.$RHPAM_VERSION_MINOR.$RHPAM_VERSION_MICRO-business-central-eap7-deployable.zip
ENV RHPAM_ADDONS=rhpam-$RHPAM_VERSION_MAJOR.$RHPAM_VERSION_MINOR.$RHPAM_VERSION_MICRO-add-ons.zip
ENV RHPAM_KIE_SERVER=rhpam-$RHPAM_VERSION_MAJOR.$RHPAM_VERSION_MINOR.$RHPAM_VERSION_MICRO-kie-server-ee8.zip
ENV RHPAM_CASE=rhpam-$RHPAM_VERSION_MAJOR.$RHPAM_VERSION_MINOR.$RHPAM_VERSION_MICRO-case-mgmt-showcase-eap7-deployable.zip

# ADD Installation Files
COPY installs/$RHPAM_DEPLOYABLE installs/$RHPAM_ADDONS installs/$RHPAM_KIE_SERVER installs/$EAP_ZIP /opt/jboss/

# Update Permissions on Installers
USER root
RUN chown 1000:1000 /opt/jboss/$EAP_ZIP /opt/jboss/$RHPAM_DEPLOYABLE /opt/jboss/$RHPAM_ADDONS /opt/jboss/$RHPAM_KIE_SERVER
USER 1000

# Prepare and run installer and cleanup installation components
RUN mkdir -p /opt/jboss/rhpam

RUN unzip -qo /opt/jboss/$EAP_ZIP -d /opt/jboss/rhpam && \
    unzip -qo /opt/jboss/$RHPAM_DEPLOYABLE -d $RHPAM_HOME/.. && \
    unzip -qo /opt/jboss/$RHPAM_KIE_SERVER -d $RHPAM_HOME/standalone/deployments && \
    touch $RHPAM_HOME/standalone/deployments/kie-server.war.dodeploy && \
    unzip -qo /opt/jboss/$RHPAM_ADDONS $RHPAM_CASE -d /opt/jboss && \
    unzip -qo /opt/jboss/$RHPAM_CASE -d $RHPAM_HOME/.. && \
    rm -rf /opt/jboss/$RHPAM_CASE /opt/jboss/$RHPAM_DEPLOYABLE /opt/jboss/$RHPAM_KIE_SERVER /opt/jboss/$RHPAM_ADDONS /opt/jboss/$EAP_ZIP && \
    touch $RHPAM_HOME/standalone/deployments/rhpam-case-mgmt-showcase.war.dodeploy && \
    $RHPAM_HOME/bin/add-user.sh -a -r ApplicationRealm -u erics -p redhatpam1! -ro analyst,admin,user,manager,kie-server,kiemgmt,rest-all --silent && \
    $RHPAM_HOME/bin/add-user.sh -a -r ApplicationRealm -u kieserver -p kieserver1! -ro kie-server,rest-all --silent && \
    $RHPAM_HOME/bin/add-user.sh -a -r ApplicationRealm -u caseuser -p redhatpam1! -ro user --silent && \
    $RHPAM_HOME/bin/add-user.sh -a -r ApplicationRealm -u casemanager -p redhatpam1! -ro user,manager --silent && \
    $RHPAM_HOME/bin/add-user.sh -a -r ApplicationRealm -u casesupplier -p redhatpam1! -ro user,supplier --silent

# Copy demo and support files
COPY support/standalone-full.xml $RHPAM_HOME/standalone/configuration/standalone.xml
COPY support/userinfo.properties $RHPAM_HOME/standalone/deployments/business-central.war/WEB-INF/classes/ 
COPY support/demo-niogit $RHPAM_HOME/bin/.niogit

# Swtich back to root user to perform build and cleanup
USER root

# Run Demo setup and cleanup
RUN chown -R jboss:jboss $RHPAM_HOME/bin/.niogit

# Run as JBoss 
USER 1000

# Expose Ports
EXPOSE 9990 9999 8080 8001

# Run BPMS
CMD ["/opt/jboss/rhpam/jboss-eap-7.3/bin/standalone.sh","-c","standalone.xml","-b", "0.0.0.0","-bmanagement","0.0.0.0"]
