#!/bin/sh 
DEMO="Signal marketing demo"
AUTHORS="Eric D. Schabell"
PROJECT="git@gitlab.com:bpmworkshop/rhpam-signal-marketing-demo.git"
PRODUCT="Red Hat Process Automation Manager"
JBOSS_HOME=./target/jboss-eap-7.3
SERVER_DIR=$JBOSS_HOME/standalone/deployments/
SERVER_CONF=$JBOSS_HOME/standalone/configuration/
SERVER_BIN=$JBOSS_HOME/bin
SRC_DIR=./installs
SUPPORT_DIR=./support
PRJ_DIR=./projects
VERSION_EAP=7.3.0
VERSION=7.11.0
EAP=jboss-eap-$VERSION_EAP.zip
RHPAM=rhpam-$VERSION-business-central-eap7-deployable.zip
RHPAM_KIE_SERVER=rhpam-$VERSION-kie-server-ee8.zip
RHPAM_ADDONS=rhpam-$VERSION-add-ons.zip
RHPAM_CASE=rhpam-$VERSION-case-mgmt-showcase-eap7-deployable.zip

# project details.
PRJ_GIT_DIR=$SUPPORT_DIR/temp-git-dir
PRJ_GIT_REPO="https://gitlab.com/bpmworkshop/rhpam-signal-marketing-repo.git"
PRJ_GIT_REPO_NAME=loan.git
NIOGIT_PRJ_GIT_REPO="MySpace/$PROJECT_GIT_REPO_NAME"

# wipe screen.
clear 

echo
echo "###################################################################"
echo "##                                                               ##"   
echo "##  Setting up the ${DEMO}                         ##"
echo "##                                                               ##"   
echo "##             ####  ##### ####     #   #  ###  #####            ##"
echo "##             #   # #     #   #    #   # #   #   #              ##"
echo "##             ####  ###   #   #    ##### #####   #              ##"
echo "##             #  #  #     #   #    #   # #   #   #              ##"
echo "##             #   # ##### ####     #   # #   #   #              ##"
echo "##                                                               ##"
echo "##           ####  ####   ###   #### #####  ####  ####           ##"
echo "##           #   # #   # #   # #     #     #     #               ##"
echo "##           ####  ####  #   # #     ###    ###   ###            ##"
echo "##           #     #  #  #   # #     #         #     #           ##"
echo "##           #     #   #  ###   #### ##### ####  ####            ##"
echo "##                                                               ##"
echo "##   ###  #   # #####  ###  #   #  ###  ##### #####  ###  #   #  ##"
echo "##  #   # #   #   #   #   # ## ## #   #   #     #   #   # ##  #  ##"
echo "##  ##### #   #   #   #   # # # # #####   #     #   #   # # # #  ##"
echo "##  #   # #   #   #   #   # #   # #   #   #     #   #   # #  ##  ##"
echo "##  #   # #####   #    ###  #   # #   #   #   #####  ###  #   #  ##"
echo "##                                                               ##"
echo "##           #   #  ###  #   #  ###  ##### ##### ####            ##"
echo "##           ## ## #   # ##  # #   # #     #     #   #           ##"
echo "##           # # # ##### # # # ##### #  ## ###   ####            ##"
echo "##           #   # #   # #  ## #   # #   # #     #  #            ##"
echo "##           #   # #   # #   # #   # ##### ##### #   #           ##"
echo "##                                                               ##" 
echo "##  brought to you by,                                           ##"   
echo "##             ${AUTHORS}                                  ##"
echo "##                                                               ##"   
echo "##  ${PROJECT}   ##"
echo "##                                                               ##"   
echo "###################################################################"
echo

# make some checks first before proceeding.	
if [ -r $SUPPORT_DIR ] || [ -L $SUPPORT_DIR ]; then
        echo "Support dir is present..."
        echo
else
        echo "$SUPPORT_DIR wasn't found. Please make sure to run this script inside the demo directory."
        echo
        exit
fi

if [ -r $SRC_DIR/$EAP ] || [ -L $SRC_DIR/$EAP ]; then
	echo "Product EAP sources are present..."
	echo
else
	echo "Need to download $EAP package from https://developers.redhat.com/products/eap/download"
	echo "and place it in the $SRC_DIR directory to proceed..."
	echo
	exit
fi

if [ -r $SRC_DIR/$RHPAM ] || [ -L $SRC_DIR/$RHPAM ]; then
	echo "Product Red Hat Process Automation Manager sources are present..."
	echo
else
	echo "Need to download $RHPAM package from https://developers.redhat.com/products/rhpam/download"
	echo "and place it in the $SRC_DIR directory to proceed..."
	echo
	exit
fi

if [ -r $SRC_DIR/$RHPAM_KIE_SERVER ] || [ -L $SRC_DIR/$RHPAM_KIE_SERVER ]; then
	echo "Product Red Hat Process Automation Manager KIE Server sources are present..."
	echo
else
	echo "Need to download $RHPAM_KIE_SERVER package from https://developers.redhat.com/products/rhpam/download"
	echo "and place it in the $SRC_DIR directory to proceed..."
	echo
	exit
fi

if [ -r $SRC_DIR/$RHPAM_ADDONS ] || [ -L $SRC_DIR/$RHPAM_ADDONS ]; then
	echo "Product Red Hat Process Automation Manager Case Management sources are present..."
	echo
else
	echo "Need to download $RHPAM_ADDONS package from https://developers.redhat.com/products/rhpam/download"
	echo "and place it in the $SRC_DIR directory to proceed..."
	echo
	exit
fi

# Remove the old JBoss instance, if it exists.
if [ -x $JBOSS_HOME ]; then
	echo "  - removing existing installation directory..."
	echo
	rm -rf ./target
fi

# Installation.
echo "JBoss EAP installation running now..."
echo
mkdir -p ./target
unzip -qo $SRC_DIR/$EAP -d ./target

if [ $? -ne 0 ]; then
	echo
	echo Error occurred during JBoss EAP installation!
	exit
fi

echo "Red Hat Process Automation Manager installation running now..."
echo
unzip -qo $SRC_DIR/$RHPAM -d target

if [ $? -ne 0 ]; then
	echo
	echo Error occurred during Red Hat Process Manager installation!
	exit
fi

echo "Red Hat Process Automation Manager Kie Server installation running now..."
echo
unzip -qo $SRC_DIR/$RHPAM_KIE_SERVER  -d $JBOSS_HOME/standalone/deployments 

if [ $? -ne 0 ]; then
	echo
	echo Error occurred during Red Hat Process Manager Kie Server installation!
	exit
fi

# Set deployment Kie Server.
touch $JBOSS_HOME/standalone/deployments/kie-server.war.dodeploy

echo "Red Hat Process Automation Manager Case Management installation running now..."
echo
unzip -qo $SRC_DIR/$RHPAM_ADDONS $RHPAM_CASE -d $SRC_DIR
unzip -qo $SRC_DIR/$RHPAM_CASE -d target
rm $SRC_DIR/$RHPAM_CASE

if [ $? -ne 0 ]; then
	echo
	echo Error occurred during Red Hat Process Manager Case Management installation!
	exit
fi

# Set deployment Case Management.
touch $JBOSS_HOME/standalone/deployments/rhpam-case-mgmt-showcase.war.dodeploy

echo "  - enabling demo accounts role setup..."
echo
$JBOSS_HOME/bin/add-user.sh -a -r ApplicationRealm -u erics -p redhatpam1! -ro analyst,admin,manager,user,kie-server,kiemgmt,rest-all --silent 
echo "  - User 'erics' password 'redhatpam1!' setup..."
echo
$JBOSS_HOME/bin/add-user.sh -a -r ApplicationRealm -u kieserver -p kieserver1! -ro kie-server,rest-all --silent
echo "  - Management user 'kieserver' password 'kieserver1!' setup..."
echo

echo "  - adding user 'caseuser' with password 'redhatpam1!'..."
echo
$JBOSS_HOME/bin/add-user.sh -a -r ApplicationRealm -u caseuser -p redhatpam1! -ro user --silent

echo "  - adding user 'casemanager' with password 'redhatpam1!'..."
echo
$JBOSS_HOME/bin/add-user.sh -a -r ApplicationRealm -u casemanager -p redhatpam1! -ro user,manager --silent

echo "  - adding user 'casesupplier' with password 'redhatpam1!'..."
echo
$JBOSS_HOME/bin/add-user.sh -a -r ApplicationRealm -u casesupplier -p redhatpam1! -ro user,supplier --silent


echo "  - setting up standalone-full.xml configuration adjustments..."
echo
cp $SUPPORT_DIR/standalone-full.xml $SERVER_CONF/standalone.xml

echo "  - setup email notification users..."
echo
cp $SUPPORT_DIR/userinfo.properties $SERVER_DIR/business-central.war/WEB-INF/classes/

# Add execute permissions to the standalone.sh script.
echo "  - making sure standalone.sh for server is executable..."
echo
chmod u+x $JBOSS_HOME/bin/standalone.sh

echo "  - copying internal provided repository first...."
echo
rm -rf $SERVER_BIN/.niogit && \
mkdir -p $SERVER_BIN/.niogit && \
cp -r $SUPPORT_DIR/demo-niogit/* $SERVER_BIN/.niogit
	
if [ $? -ne 0 ]; then
	echo
	echo "Error occurred setting up provided project repository!"
	exit
fi

echo "  - trying to pull the project's repository from: $PRJ_GIT_REPO"
echo
rm -rf ./target/temp
git clone $PRJ_GIT_REPO ./target/temp/$PRJ_GIT_REPO_NAME

if [ $? -ne 0 ]; then
	echo
	echo "Error occurred trying to pull the project's remote repository!"
	echo "Note: project repository not reachable, so continuing with provided repository!"
	echo

else
		
	echo
	echo "  - making bare clone of project repo: $PRJ_GIT_DIR/$PRJ_GIT_REPO_NAME ..."
	echo
	rm -rf $PRJ_GIT_DIR/$PRJ_GIT_REPO_NAME && \
	mkdir -p $PRJ_GIT_DIR && \
	git clone --bare target/temp/$PRJ_GIT_REPO_NAME $PRJ_GIT_DIR/$PRJ_GIT_REPO_NAME && \
  rm -rf ./target/temp
		
	if [ $? -ne 0 ]; then
		echo
		echo "Error occurred trying to make bare clone of project repository!"
		exit
	fi
		
  # cleanup temp directory.
	echo
	echo "  - copy the repo to the installation directory..."
	echo
	rm -rf $SERVER_BIN/.niogit/MySpace/*.git && \
	cp -R $PRJ_GIT_DIR/$PRJ_GIT_REPO_NAME $SERVER_BIN/.niogit/$NIOGIT_PRJ_GIT_REPO

	if [ $? -ne 0 ]; then
		echo
		echo "Error occurred trying to copy repo to installation directory!"
		exit
	fi

	rm -rf $PRJ_GIT_DIR
fi

echo
echo "========================================================================"
echo "=                                                                      ="
echo "=  You can now start the $PRODUCT with:      ="
echo "=                                                                      ="
echo "=        $SERVER_BIN/standalone.sh                      ="
echo "=                                                                      ="
echo "=  Login to Red Hat Process Automation Manager to start exploring the  ="
echo "=  project at:                                                         ="
echo "=                                                                      ="
echo "=        http://localhost:8080/business-central                        ="
echo "=                                                                      ="
echo "=    Log in: [ u:erics / p:redhatpam1! ]                               ="
echo "=                                                                      ="
echo "=    Others:                                                           ="
echo "=            [ u:kieserver / p:redhatpam1! ]                           ="
echo "=            [ u:caseuser / p:redhatpam1! ]                            ="
echo "=            [ u:casemanager / p:redhatpam1! ]                         ="
echo "=            [ u:casesupplier / p:redhatpam1! ]                        ="
echo "=                                                                      ="
echo "=  Explore the KieServer API docs here:                                ="
echo "=                                                                      ="
echo "=        http://localhost:8080/kie-server/docs/                        ="
echo "=                                                                      ="
echo "========================================================================"
echo

